package com.practice.chapter3
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

//,

class FoodAdapter(private val context: Context, private val foodList: List<FoodItem>, private val onItemClick: (FoodItem) -> Unit) :
    RecyclerView.Adapter<FoodAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val foodImage: ImageView = itemView.findViewById(R.id.food_image)
        val foodName: TextView = itemView.findViewById(R.id.food_name)
        val foodPrice: TextView = itemView.findViewById(R.id.food_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item_food, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val foodItem = foodList[position]

        //text-nya
        holder.foodName.text = foodItem.name
        holder.foodPrice.text = foodItem.price

        // poto2nya biar ga pake R.
        val resourceId = context.resources.getIdentifier(
            foodItem.imageResourceName, "drawable", context.packageName
        )
        holder.foodImage.setImageResource(resourceId)

        holder.itemView.setOnClickListener {
            onItemClick(foodItem)
        }
    }

    override fun getItemCount(): Int {
        return foodList.size
    }
}
