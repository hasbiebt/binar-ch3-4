package com.practice.chapter3

data class FoodItem(
    val name: String,
    val price: String,
    val imageResourceName: String
)
