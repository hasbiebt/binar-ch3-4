package com.practice.chapter3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fm = supportFragmentManager
        val transaction = fm.beginTransaction()

        val fragmentMain = MainFragment()
        val fragmentSecond = SecondaryFragment()

        transaction.add(R.id.container_fragment_1, fragmentMain)
        transaction.add(R.id.container_fragment_2, fragmentSecond)
        transaction.commit()


    }
}
