package com.practice.chapter3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TestFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val foodList = listOf(
            FoodItem("Ayam Korea", "35.000", "ayam_bakar"),
            FoodItem("Burger Lezat", "25.000", "burger_murah"),
            FoodItem("Dimsum Enak", "45.000", "dimsum_enak"),
            FoodItem("Spaghetti Mahal", "45.000", "sepageti"),
            FoodItem("Dimsum Enak", "45.000", "dimsum_enak"),
            FoodItem("Spaghetti Mahal", "45.000", "sepageti"),
            FoodItem("Dimsum Enak", "45.000", "dimsum_enak"),
            FoodItem("Spaghetti Mahal", "45.000", "sepageti")
        )

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.adapter = FoodAdapter(requireContext(), foodList) { foodItem ->
            val fragment = FragmentFoodDetailed()
            val args = Bundle()
            args.putString("foodName", foodItem.name)
            args.putString("foodPrice", foodItem.price)
            args.putString("foodImageResource", foodItem.imageResourceName)
            fragment.arguments = args

            parentFragmentManager.beginTransaction()
                .replace(R.id.container_fragment_2, fragment)
                .addToBackStack(null)
                .commit()
        }
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }
}
